package application.graphic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import application.engine.CardLine;
import application.engine.Carte;
import application.engine.TimeLine;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ButtonPlayedCards extends Button {
	private Carte tl;
	private PlateauController plc;
	private int position;
	
	public ButtonPlayedCards(PlateauController pc,int pos) {
		plc=pc;
		position=pos;
		this.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	try {
					plc.placerCarte(position);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
            }
        });	
	}
	
	public ButtonPlayedCards(Carte tl,boolean b) throws FileNotFoundException {
		this.tl=tl;
		FileInputStream fis;
		
		if(b) {
			fis=new FileInputStream(chemin_vc(((CardLine) tl).getImage()));
		} else {
			fis=new FileInputStream(chemin_vt(((TimeLine) tl).getImage()));
		}				
		Image image = new Image(fis);
		this.setGraphic(new ImageView(image));
	}
	
	
	

	public void setPosition(int position) {
		this.position = position;
	}

	public Carte getTl() {
		return tl;
	}

	public String chemin_vt(String s) {
		return ".\\data\\timeline\\cards\\"+s+"_date.jpeg";
	}
	
	public String chemin_vc(String s) {
		return ".\\data\\cardline\\cards\\"+s+"_reponse.jpeg";
	}
}
