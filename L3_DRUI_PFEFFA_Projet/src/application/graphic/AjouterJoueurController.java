package application.graphic;

import application.engine.Joueur;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AjouterJoueurController {
	
	private Joueur joueur;
	private Stage dialogStage;
	private boolean valide;
	
	@FXML
	private Button ajouter;
	@FXML
	private TextField prenom;
	@FXML
	private TextField age;
	
	public AjouterJoueurController(){
		
	}
	
	@FXML
	private void initialize() {
		
	}
	
	public void setDialogStage(Stage ds) {
		dialogStage=ds;
	}
	
	public void setJoueur(Joueur j) {
		joueur=j;
	}
	
	@FXML
	public boolean valider() {
		if(!prenom.getText().isEmpty()) {
			if(!age.getText().isEmpty()) {
				try {
					Integer.parseInt(age.getText());
				} catch (NumberFormatException e) {
					Alert alert=new Alert(AlertType.ERROR);
					alert.initOwner(dialogStage);
					alert.setTitle("Erreur");
					alert.setContentText("Veuillez ins�rer un age valide");
					alert.showAndWait();
					return false;
				}
				joueur.setPseudo(prenom.getText());
				joueur.setAge(age.getText());
				dialogStage.close();
				return true;
			} else {
				Alert alert=new Alert(AlertType.ERROR);
				alert.initOwner(dialogStage);
				alert.setTitle("Erreur");
				alert.setContentText("Veuillez ins�rer un age valide");
				alert.showAndWait();
			}
		} else {
			Alert alert=new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Erreur");
			alert.setContentText("Veuillez ins�rer un nom");
			alert.showAndWait();
		}
		return false;
	}
	
	public boolean isValide() {
		return valide;
	}
	
}
