package application.graphic;

import application.Main;
import application.engine.Joueur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;

public class MenuController {
	private Main main;

	
	@FXML
	private ToggleGroup tg1;
	@FXML
	private Button jouer;
	@FXML
	private Button ajouter;
	@FXML
	private Button supprimer;
	@FXML
	private RadioButton rb_TimeLine;
	@FXML
	private RadioButton rb_CardLine;
	@FXML
	private TableView<Joueur> tab_joueur;
	@FXML
	private TableColumn<Joueur,String> name;
	@FXML
	private TableColumn<Joueur,String> age;
	@FXML
	private ComboBox critere_cardline;

	public MenuController() {
		
	}

	@FXML
	private void initialize() {
		name.setCellValueFactory(cellData -> cellData.getValue().getPseudoProperty());		
		age.setCellValueFactory(cellData -> cellData.getValue().getAgeProperty());
		ObservableList<String> list = FXCollections.observableArrayList("pib","pollution","population","superficie");		
		critere_cardline.setItems(list);
	}
	
	@FXML
	private void ajouter_joueur() {		
		Joueur temp = new Joueur("","");
		boolean ok = main.showAjoutJoueur(temp);
		if (ok) {
			main.getPartie().getListe_joueurs().add(temp);
			System.out.println("Joueur ajout�");
		} else {
			System.err.println("Bug");
		}
	}
	
	@FXML
	private void supprimer_joueur() {
		int index = tab_joueur.getSelectionModel().getSelectedIndex();
		if(index>=0) {
			tab_joueur.getItems().remove(index);
		}
	}

	@FXML
	private void jouer() throws Exception {
		if(rb_CardLine.isSelected()) {
			if(main.getPartie().getListe_joueurs().size()>=2) {
				main.getPartie().setIscardline(true);
				main.getPartie().setCritere((String) critere_cardline.getValue());
				main.getPartie().premier_joueur();
				main.getPartie().ini_cartes_cardline();
				main.showPlateau();
			}			
		} else {
			if(main.getPartie().getListe_joueurs().size()>=2) {
				main.getPartie().premier_joueur();
				main.getPartie().ini_cartes_timeline();
				main.showPlateau();
			}
		}
	}

	public void setMainApp(Main m) {
		main=m;
		tab_joueur.setItems(m.getPartie().getListe_joueurs());
	}

}
