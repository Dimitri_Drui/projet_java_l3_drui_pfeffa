package application.graphic;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import application.Main;
import application.engine.CardLine;
import application.engine.Joueur;
import application.engine.TimeLine;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class PlateauController {

	private Main main;
	private ButtonHandCards selected_card;
	private Joueur current_player;

	ArrayList<ButtonPlayedCards> lbpc=new ArrayList<ButtonPlayedCards>();

	@FXML
	private Label lbl_pseudo;
	@FXML
	private Label lbl_score;
	@FXML
	private GridPane grid_played_cards;
	@FXML
	private GridPane grid_hand;

	public PlateauController() {

	}

	public void update_pseudo(String s) {
		lbl_pseudo.setText(s);
	}

	public void update_score(String s) {
		lbl_score.setText(s);
	}

	public void setMainApp(Main m) {
		main=m;
	}

	@FXML
	public void placerCarte(int position) throws FileNotFoundException {
		if(selected_card!=null) {
			if(main.getPartie().Iscardline()) {
				//Partie CardLine
				if(main.getPartie().getCritere().equals("pib")) {
					if(position==0) {
						if(lbpc.get(1).getTl().test_pib_gauche(((CardLine) selected_card.getTl()).getPib())) {
							main.getPartie().victory(selected_card.getTl(),current_player);
							ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
							ButtonPlayedCards bpc2=new ButtonPlayedCards(this,2);
							lbpc.add(1, bpc);
							lbpc.add(2, bpc2);
							grid_played_cards.addRow(0, bpc2);
							grid_played_cards.addRow(0, bpc);
						} else {
							main.getPartie().lost(selected_card.getTl(),current_player);
						}
					} else {
						if(position==lbpc.size()-1) {
							if(lbpc.get(lbpc.size()-2).getTl().test_pib_droite(((CardLine) selected_card.getTl()).getPib())) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,0);
								lbpc.add(bpc);
								lbpc.add(bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						} else {
							if(lbpc.get(position+1).getTl().test_pib_gauche(((CardLine) selected_card.getTl()).getPib())&&(lbpc.get(position-1).getTl().test_pib_droite(((CardLine) selected_card.getTl()).getPib()))) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,position+1);
								lbpc.add(position+1,bpc);
								lbpc.add(position+2,bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						}
					}
				}
				if(main.getPartie().getCritere().equals("pollution")) {
					if(position==0) {
						if(lbpc.get(1).getTl().test_pol_gauche(((CardLine) selected_card.getTl()).getPollution())) {
							main.getPartie().victory(selected_card.getTl(),current_player);
							ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
							ButtonPlayedCards bpc2=new ButtonPlayedCards(this,2);
							lbpc.add(1, bpc);
							lbpc.add(2, bpc2);
							grid_played_cards.addRow(0, bpc2);
							grid_played_cards.addRow(0, bpc);
						} else {
							main.getPartie().lost(selected_card.getTl(),current_player);
						}
					} else {
						if(position==lbpc.size()-1) {
							if(lbpc.get(lbpc.size()-2).getTl().test_pol_droite(((CardLine) selected_card.getTl()).getPollution())) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,0);
								lbpc.add(bpc);
								lbpc.add(bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						} else {
							if(lbpc.get(position+1).getTl().test_pol_gauche(((CardLine) selected_card.getTl()).getPollution())&&(lbpc.get(position-1).getTl().test_pol_droite(((CardLine) selected_card.getTl()).getPollution()))) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,position+1);
								lbpc.add(position+1,bpc);
								lbpc.add(position+2,bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						}
					}
				}
				if(main.getPartie().getCritere().equals("population")) {
					if(position==0) {
						if(lbpc.get(1).getTl().test_pop_gauche(((CardLine) selected_card.getTl()).getPopulation())) {
							main.getPartie().victory(selected_card.getTl(),current_player);
							ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
							ButtonPlayedCards bpc2=new ButtonPlayedCards(this,2);
							lbpc.add(1, bpc);
							lbpc.add(2, bpc2);
							grid_played_cards.addRow(0, bpc2);
							grid_played_cards.addRow(0, bpc);
						} else {
							main.getPartie().lost(selected_card.getTl(),current_player);
						}
					} else {
						if(position==lbpc.size()-1) {
							if(lbpc.get(lbpc.size()-2).getTl().test_pop_droite(((CardLine) selected_card.getTl()).getPopulation())) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,0);
								lbpc.add(bpc);
								lbpc.add(bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						} else {
							if(lbpc.get(position+1).getTl().test_pop_gauche(((CardLine) selected_card.getTl()).getPopulation())&&(lbpc.get(position-1).getTl().test_pop_droite(((CardLine) selected_card.getTl()).getPopulation()))) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,position+1);
								lbpc.add(position+1,bpc);
								lbpc.add(position+2,bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						}
					}
				}
				if(main.getPartie().getCritere().equals("superficie")) {
					if(position==0) {
						if(lbpc.get(1).getTl().test_sup_gauche(((CardLine) selected_card.getTl()).getSuperficie())) {
							main.getPartie().victory(selected_card.getTl(),current_player);
							ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
							ButtonPlayedCards bpc2=new ButtonPlayedCards(this,2);
							lbpc.add(1, bpc);
							lbpc.add(2, bpc2);
							grid_played_cards.addRow(0, bpc2);
							grid_played_cards.addRow(0, bpc);
						} else {
							main.getPartie().lost(selected_card.getTl(),current_player);
						}
					} else {
						if(position==lbpc.size()-1) {
							if(lbpc.get(lbpc.size()-2).getTl().test_sup_droite(((CardLine) selected_card.getTl()).getSuperficie())) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,0);
								lbpc.add(bpc);
								lbpc.add(bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						} else {
							if(lbpc.get(position+1).getTl().test_sup_gauche(((CardLine) selected_card.getTl()).getSuperficie())&&(lbpc.get(position-1).getTl().test_sup_droite(((CardLine) selected_card.getTl()).getSuperficie()))) {
								main.getPartie().victory(selected_card.getTl(),current_player);
								ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
								ButtonPlayedCards bpc2=new ButtonPlayedCards(this,position+1);
								lbpc.add(position+1,bpc);
								lbpc.add(position+2,bpc2);
								grid_played_cards.addRow(0, bpc2);
								grid_played_cards.addRow(0, bpc);
							} else {
								main.getPartie().lost(selected_card.getTl(),current_player);
							}
						}
					}
				}
			}else {
				// partie Timeline
				if(position==0) {
					if(lbpc.get(1).getTl().test_date_gauche(((TimeLine) selected_card.getTl()).getDate())) {
						main.getPartie().victory(selected_card.getTl(),current_player);
						ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
						ButtonPlayedCards bpc2=new ButtonPlayedCards(this,2);
						lbpc.add(1, bpc);
						lbpc.add(2, bpc2);
						grid_played_cards.addRow(0, bpc2);
						grid_played_cards.addRow(0, bpc);
					} else {
						main.getPartie().lost(selected_card.getTl(),current_player);
					}
				} else {
					if(position==lbpc.size()-1) {
						if(lbpc.get(lbpc.size()-2).getTl().test_date_droite(((TimeLine) selected_card.getTl()).getDate())) {
							main.getPartie().victory(selected_card.getTl(),current_player);
							ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
							ButtonPlayedCards bpc2=new ButtonPlayedCards(this,0);
							lbpc.add(bpc);
							lbpc.add(bpc2);
							grid_played_cards.addRow(0, bpc2);
							grid_played_cards.addRow(0, bpc);
						} else {
							main.getPartie().lost(selected_card.getTl(),current_player);
						}
					} else {
						if(lbpc.get(position+1).getTl().test_date_gauche(((TimeLine) selected_card.getTl()).getDate())&&(lbpc.get(position-1).getTl().test_date_droite(((TimeLine) selected_card.getTl()).getDate()))) {
							main.getPartie().victory(selected_card.getTl(),current_player);
							ButtonPlayedCards bpc=new ButtonPlayedCards(selected_card.getTl(),main.getPartie().Iscardline());
							ButtonPlayedCards bpc2=new ButtonPlayedCards(this,position+1);
							lbpc.add(position+1,bpc);
							lbpc.add(position+2,bpc2);
							grid_played_cards.addRow(0, bpc2);
							grid_played_cards.addRow(0, bpc);
						} else {
							main.getPartie().lost(selected_card.getTl(),current_player);
						}
					}

				}
			}
		}	
		if(main.getPartie().win_condition()) {
			main.fin_de_partie(current_player);
		}

		if(main.getPartie().getListe_joueurs().indexOf(current_player)==main.getPartie().getListe_joueurs().size()-1) {
			current_player=main.getPartie().getListe_joueurs().get(0);
		} else {
			current_player=main.getPartie().getListe_joueurs().get(main.getPartie().getListe_joueurs().indexOf(current_player)+1);
		}

		for(int i=0;i<lbpc.size();i++) {
			lbpc.get(i).setPosition(i);
		}
		afficher_cartes();
	}

	@FXML
	public void selectionnerCarte(ButtonHandCards bhc) {
		selected_card=bhc;
	}

	@FXML
	public void initialize() {

	}

	public void init() throws FileNotFoundException {
		current_player=main.getPartie().first_player();
		main.getPartie().premiere_carte();		
		ButtonPlayedCards played_left=new ButtonPlayedCards(this,0);
		ButtonPlayedCards fc = new ButtonPlayedCards(main.getPartie().getPlayed_cards().get(0),main.getPartie().Iscardline());
		ButtonPlayedCards played_right=new ButtonPlayedCards(this,2);
		lbpc.add(played_left);
		lbpc.add(fc);
		lbpc.add(played_right);
		afficher_cartes();		
	}

	public void afficher_cartes() throws FileNotFoundException {
		grid_played_cards.getChildren().clear();
		for(int i=0;i<lbpc.size();i++) {
			grid_played_cards.add(lbpc.get(i), i, 0);
		}
		grid_hand.getChildren().clear();
		for(int i=0;i<current_player.getMain().size();i++) {
			grid_hand.add(new ButtonHandCards(this,current_player.getMain().get(i), main.getPartie().Iscardline()),i,0);
		}
		update_pseudo(current_player.getPseudo());
		update_score(current_player.getScore());
	}
}
