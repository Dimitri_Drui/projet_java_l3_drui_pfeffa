package application.graphic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import application.engine.TimeLine;
import application.engine.CardLine;
import application.engine.Carte;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ButtonHandCards extends Button {
	private Carte tl;
	private PlateauController plc;
	
	public ButtonHandCards(PlateauController pc,Carte tl,boolean b) throws FileNotFoundException {
		ButtonHandCards ref=this;
		this.tl=tl;
		plc=pc;
		FileInputStream fis;
		if(b) {
			fis=new FileInputStream(chemin_rc(((CardLine) tl).getImage()));
		} else {
			fis=new FileInputStream(chemin_rt(((TimeLine) tl).getImage()));
		}
	
		Image image=new Image(fis);
		this.setGraphic(new ImageView(image));
		this.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
              plc.selectionnerCarte(ref);
            }
        });	
	}
	
	public Carte getTl() {
		return tl;
	}

	public String chemin_rt(String s) {
		return ".\\data\\timeline\\cards\\"+s+".jpeg";
	}
	public String chemin_rc(String s) {
		return ".\\data\\cardline\\cards\\"+s+".jpeg";
	}

}
