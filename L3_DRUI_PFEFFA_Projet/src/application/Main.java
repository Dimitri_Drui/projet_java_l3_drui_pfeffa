package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

import application.engine.Joueur;
import application.engine.Partie;
import application.graphic.AjouterJoueurController;
import application.graphic.MenuController;
import application.graphic.PlateauController;

public class Main extends Application {

	private static Partie partie;
	private Stage main_stage;
	private BorderPane rootLayout;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		main_stage=primaryStage;
		main_stage.setTitle("TimeLine");
		partie=new Partie();
		initRootLayout();
		showMenu();
	}

	public void initRootLayout() {
		try {
			FXMLLoader loader=new FXMLLoader();
			loader.setLocation(Main.class.getResource("graphic/RootLayout.fxml"));
			rootLayout=(BorderPane)loader.load();

			Scene scene=new Scene(rootLayout);
			main_stage.setScene(scene);
			main_stage.show();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void showMenu() {
		try {
			FXMLLoader loader=new FXMLLoader();
			loader.setLocation(Main.class.getResource("graphic/Menu.fxml"));
			AnchorPane menu=(AnchorPane)loader.load();
			menu.setPrefWidth(600);
			menu.setPrefHeight(400);
			rootLayout.setCenter(menu);

			MenuController controller=loader.getController();
			controller.setMainApp(this);
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public boolean showAjoutJoueur(Joueur j) {
		try {
			FXMLLoader loader=new FXMLLoader();
			loader.setLocation(Main.class.getResource("graphic/AjouterJoueurPanel.fxml"));
			AnchorPane ajout=(AnchorPane) loader.load();

			Stage dialogStage=new Stage();
			dialogStage.setTitle("Ajouter joueur");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(main_stage);
			Scene scene=new Scene(ajout);
			dialogStage.setScene(scene);

			AjouterJoueurController ajc=loader.getController();
			ajc.setDialogStage(dialogStage);
			ajc.setJoueur(j);
			dialogStage.showAndWait();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean showPlateau() {
		try {
			FXMLLoader loader=new FXMLLoader();
			loader.setLocation(Main.class.getResource("graphic/PlateauTimeLine.fxml"));

			AnchorPane plateau=(AnchorPane)loader.load();
			rootLayout.setCenter(plateau);

			PlateauController controller=loader.getController();
			controller.setMainApp(this);
			controller.init();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public Partie getPartie() {
		return partie;
	}

	public void fin_de_partie(Joueur current_player) {
		Alert alert=new Alert(AlertType.INFORMATION);
		alert.initOwner(main_stage);
		alert.setTitle("VICTOIRE !");
		alert.setContentText("Félicitations joueur "+current_player.getPseudo()+"! Vous avez gagné !");
		alert.showAndWait();
		main_stage.close();
	}



}
