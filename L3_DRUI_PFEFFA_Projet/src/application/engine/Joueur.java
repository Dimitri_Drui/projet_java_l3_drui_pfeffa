package application.engine;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Joueur {
	private StringProperty pseudo;
	private StringProperty age;
	private ArrayList<Carte> main=new ArrayList<Carte>();
	private StringProperty score;

	public Joueur(String pse, String a) {
		pseudo=new SimpleStringProperty(pse);
		age=new SimpleStringProperty(a);
		score=new SimpleStringProperty("0");
	}

	public boolean aGagne() {
		if(main.size()==0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void ajouter_carte_main(Carte c) {
		main.add(c);
	}

	public void poseUneCarte(Carte carte) {
		main.remove(carte);
	}

	
	
	//getter 
	public StringProperty getPseudoProperty() {
		return pseudo;
	}
	
	public StringProperty getAgeProperty() {
		return age;
	}
	
	public StringProperty getScoreProperty() {
		return score;
	}
	
	public String getPseudo() {
		return pseudo.get();
	}

	public String getAge() {
		return age.get();
	}
	
	public String getScore() {
		return score.get();
	}
	
	//setter
	public void setPseudo(String pseudo) {
		this.pseudo.setValue(pseudo);
	}

	public void setAge(String age) {
		this.age.setValue(age);
	}

	public void setScore(String score) {
		this.score.setValue(score);
	}
	
	public int getNbCartes() {
		return main.size();
	}

	public ArrayList<Carte> getMain() {
		return main;
	}

	

	@Override
	public String toString() {
		return "Joueur [pseudo=" + pseudo + ", age=" + age + "]";
	}
	
	
	
}
