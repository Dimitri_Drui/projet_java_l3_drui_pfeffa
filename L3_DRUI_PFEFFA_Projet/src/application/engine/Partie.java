package application.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import application.cardcomp.*;
import application.cardcomp.TimeComparator;
import application.readercsv.Reader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;



public class Partie {

	private ArrayList<Carte> discard_pile=new ArrayList<Carte>();
	private ArrayList<Carte> paquet=new ArrayList<Carte>();
	private ArrayList<Carte> played_cards=new ArrayList<Carte>();
	private ObservableList<Joueur> liste_joueurs = FXCollections.observableArrayList();
	private boolean iscardline=false; //repr�sente le type de partie
	private String critere=""; // critere r�cup�r� du joueur pour la partie
	
	public Partie() {
		
	}
	
	// d�termine si la win condition est atteinte et donne le gagnant s'il y en a un
	public boolean win_condition() {
		for(Joueur j : liste_joueurs) {
			if(j.aGagne()) {
				//System.out.println("Le joueur : "+j.toString()+" a gagn� !");
				return true;
			}
		} return false;
	}

	// d�termine qui doit jouer le premier en fonction de son age
	public Joueur first_player() {
		int min=Integer.parseInt(liste_joueurs.get(0).getAge());
		Joueur res=liste_joueurs.get(0);
		for(Joueur j : liste_joueurs) {
			if(min>Integer.parseInt(j.getAge())) {
				min=Integer.parseInt(j.getAge());
				res=j;
			}
		}
		return res;		
	}

	public int nb_joueurs() {
		return liste_joueurs.size();
	}

	// d�termine le nombre de cartes � piocher en fonction du nombre de joueurs pr�sents
	public int nb_to_pick() throws Exception {
		if(nb_joueurs()<=3) {
			return 6;
		} else {
			if(nb_joueurs()<=5) {
				return 5;
			} else {
				if(nb_joueurs()<=8) {
					return 4;
				}
			}
		}
		throw new Exception("Le nombre de cartes d�passe l'entendement.");		
	}

	// faire piocher une carte du paquet au joueur
	public void pick_one_card(Joueur j) {
		reset_paquet_if_needed();
		j.getMain().add(paquet.get(0));
		paquet.remove(0);
	}

	// m�lange les cartes
	public void random_shuffle(ArrayList<Carte> l_c) {
		Collections.shuffle(l_c);
	}

	// fait piocher les cartes � chaque joueurs
	// pb les joueurs ont TOUS les m�mes cartes
	public void card_pick() throws Exception {		
		for(Joueur j : liste_joueurs) {
			for(int i=0;i<nb_to_pick();i++) {
				j.ajouter_carte_main(paquet.get(i));
				paquet.remove(i);
			}
		}
	}

	// met � jour la liste des joueurs pour les d�caler en fonction du premier
	public void update_list(Joueur first) {		
		int ref=liste_joueurs.indexOf(first);
		Joueur temp = liste_joueurs.get(ref);
		Joueur temp2 = liste_joueurs.get(0);
		liste_joueurs.set(ref, temp2);
		liste_joueurs.set(0, temp);
	}

	// si le paquet est vide >> prendre la d�fausse shuffle+
	public void reset_paquet_if_needed() {
		if(paquet.isEmpty()) {
			random_shuffle(discard_pile);
			paquet.addAll(discard_pile);
		}
	}

	//trie les cartes jou�es pour timeline
	public void sort_played_card_timeline() {
		Comparator<Carte> c=new TimeComparator();
		getPlayed_cards().sort(c);
	}
	
	//trie les cartes jou�es pour cardline selon crit�re choisi
	public void sort_played_card_cardline(String s) {
		if(s.equals("pib")) {
			Comparator<Carte> c=new CardComparatorPib();
			getPlayed_cards().sort(c);
		}
		if(s.equals("pollution")) {
			Comparator<Carte> c=new CardComparatorPol();
			getPlayed_cards().sort(c);
		}
		if(s.equals("population")) {
			Comparator<Carte> c=new CardComparatorPop();
			getPlayed_cards().sort(c);
		}		
		if(s.equals("superficie")) {
			Comparator<Carte> c=new CardComparatorSup();
			getPlayed_cards().sort(c);
		}		
	}
	
	public void ini_joueurs(ArrayList<Joueur> l) {
		liste_joueurs.addAll(l);
	}
	
	public void premier_joueur() {
		Joueur first=first_player();
		update_list(first);
	}
	
	public void ini_cartes_timeline() throws Exception {
		Reader r=new Reader();
		paquet=r.getListe_cartes();
		random_shuffle(paquet);
		card_pick();
	}
	
	public void ini_cartes_cardline() throws Exception {
		Reader r=new Reader(2);
		paquet=r.getListe_cartes();
		random_shuffle(paquet);
		card_pick();
	}
	
	public void premiere_carte() {
		played_cards.add(paquet.get(0));
		paquet.remove(0);
	}

	public ArrayList<Carte> getDiscard_pile() {
		return discard_pile;
	}

	public ArrayList<Carte> getPaquet() {
		return paquet;
	}

	public ObservableList<Joueur> getListe_joueurs() {
		return liste_joueurs;
	}

	public ArrayList<Carte> getPlayed_cards() {
		return played_cards;
	}

	// Plateau Controller
	public void victory(Carte c,Joueur j) {
		played_cards.add(c);
		j.getMain().remove(j.getMain().indexOf(c));
		j.setScore(""+(Integer.parseInt(j.getScore())+1));
	}
	
	public void lost(Carte c,Joueur j) {
		discard_pile.add(c);
		j.getMain().remove(j.getMain().indexOf(c));
		pick_one_card(j);
	}

	public boolean Iscardline() {
		return iscardline;
	}

	public void setIscardline(boolean iscardline) {
		this.iscardline = iscardline;
	}

	public String getCritere() {
		return critere;
	}

	public void setCritere(String critere) {
		this.critere = critere;
	}
}