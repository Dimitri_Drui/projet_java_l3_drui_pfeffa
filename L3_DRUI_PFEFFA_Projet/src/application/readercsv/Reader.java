package application.readercsv;
import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;

import application.engine.*;



// virer typecarte
// faire de carte une classe m�re
// deux classes filles timeline et cardline
// deux constructeur dans reader
// chemin vers le fichier en final

public class Reader {
	
	private final String FILE_PATH_TIME=".\\data\\timeline.csv";
	private final String FILE_PATH_CARD=".\\data\\cardline.csv";
	private ArrayList<Carte> liste_cartes=new ArrayList<Carte>();

	// s�parateur c'est la ','
	//le constructeur lit le fichier voulu sans faire de traitement
	// int c sert simplement � d�terminer si il faut cr�er des cartes Timeline ou des cartes Cardline
	// 1er pour timeline, 2e pour cardline
	public Reader() throws FileNotFoundException,IOException, ParseException {
		String line = "";
		BufferedReader br = new BufferedReader(new FileReader(FILE_PATH_TIME));
		br.readLine(); // on saute la premi�re ligne de toute fa�on
		//lit ligne par ligne
		while ((line = br.readLine()) != null) {
			String[] current = line.split(";");
			TimeLine t=new TimeLine(current[0],toInt(current[1]),current[2]);						
			liste_cartes.add(t);
		}
		br.close();
	}
	
	public Reader(int i) throws FileNotFoundException,IOException, ParseException {
		String line = "";
		BufferedReader br = new BufferedReader(new FileReader(FILE_PATH_CARD));
		//lit ligne par ligne
		br.readLine(); // on saute la premi�re ligne de toute fa�on

		while ((line = br.readLine()) != null) {
			String[] current = line.split(",");
			CardLine t=new CardLine(current[0],toDouble(current[1]),toDouble(current[2]),toDouble(current[3]),toDouble(current[4]),current[5],current[6]);
			liste_cartes.add(t);
		}
		br.close();
	}

	public double toDouble(String s) {
		return new Double(s);
	}

	public int toInt(String s) {
		return new Integer(s);
	}

	public ArrayList<Carte> getListe_cartes() {
		return liste_cartes;
	}

}