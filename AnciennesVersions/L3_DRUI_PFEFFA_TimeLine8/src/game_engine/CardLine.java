package game_engine;

public class CardLine extends Carte {
	private String ville;
	private double superficie;
	private double population;
	private double pib;
	private double pollution;
	private String image;
	private String nom_pays;
	
	public CardLine(String v,double s,double pop,double pi,double pol,String i,String n) {
		super();
		ville=v;
		superficie=s;
		population=pop;
		pib=pi;
		pollution=pol;
		image=i;
		nom_pays=n;
	}

	public String getVille() {
		return ville;
	}

	public double getSuperficie() {
		return superficie;
	}

	public double getPopulation() {
		return population;
	}

	public double getPib() {
		return pib;
	}

	public double getPollution() {
		return pollution;
	}

	public String getImage() {
		return image;
	}

	public String getnom_pays() {
		return nom_pays;
	}
	
	public String toStringRecto() {
		return "[ "+nom_pays+" ]";
	}
	
	public String toStringVerso() {
		return "[ "+ville+" ; "+String.valueOf(superficie)+" ; "+String.valueOf(population)+" ; "+String.valueOf(pib)+" ; "+String.valueOf(pollution)+" ; "+String.valueOf(population)+" ; "+nom_pays+" ]";
	}
	
}
