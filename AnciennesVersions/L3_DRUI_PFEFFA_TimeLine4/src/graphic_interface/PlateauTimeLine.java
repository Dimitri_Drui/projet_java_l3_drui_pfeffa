package graphic_interface;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import game_engine.Partie;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JTextField;
import javax.swing.JButton;


public class PlateauTimeLine extends JFrame {

	private JPanel contentPane;
	private JTextField place;
	private JTextField numcarte;

	/*
	 * A EFFACER C'EST JUSTE POUR TESTER
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlateauTimeLine frame = new PlateauTimeLine();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PlateauTimeLine() {
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1000, 500);
		this.setTitle("Plateau de Jeu TimeLine");
		this.setLocation((screen.width-this.getSize().width)/2, (screen.height-this.getSize().height)/2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCartesJoues = new JLabel("Cartes jou\u00E9es :");
		lblCartesJoues.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCartesJoues.setBounds(10, 11, 130, 33);
		contentPane.add(lblCartesJoues);
		
		JLabel lblVotreMain = new JLabel("Votre main :");
		lblVotreMain.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVotreMain.setBounds(254, 437, 103, 14);
		contentPane.add(lblVotreMain);
		
		JLabel lblNomDuJoueur = new JLabel("Nom du Joueur :");
		lblNomDuJoueur.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNomDuJoueur.setBounds(10, 437, 145, 14);
		contentPane.add(lblNomDuJoueur);
		
		JLabel lblaff_namejoueur = new JLabel("");
		lblaff_namejoueur.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblaff_namejoueur.setBounds(150, 437, 94, 14);
		contentPane.add(lblaff_namejoueur);
		
		JLabel contimghand_1 = new JLabel("");
		contimghand_1.setBounds(387, 322, 90, 150);
		contentPane.add(contimghand_1);
		
		JLabel contimghand_2 = new JLabel("");
		contimghand_2.setBounds(484, 322, 90, 150);
		contentPane.add(contimghand_2);
		
		JLabel contimghand_3 = new JLabel("");
		contimghand_3.setBounds(582, 322, 90, 150);
		contentPane.add(contimghand_3);
		
		JLabel contimghand_4 = new JLabel("");
		contimghand_4.setBounds(680, 322, 90, 150);
		contentPane.add(contimghand_4);
		
		JLabel contimghand_5 = new JLabel("");
		contimghand_5.setBounds(778, 322, 90, 150);
		contentPane.add(contimghand_5);
		
		JLabel lblNewLabel = new JLabel("G/D ?");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(254, 353, 54, 14);
		contentPane.add(lblNewLabel);
		
		place = new JTextField();
		place.setBounds(254, 378, 86, 20);
		contentPane.add(place);
		place.setColumns(10);
		
		JLabel lblNCarte = new JLabel("N\u00B0 Carte ?");
		lblNCarte.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNCarte.setBounds(254, 293, 86, 14);
		contentPane.add(lblNCarte);
		
		numcarte = new JTextField();
		numcarte.setBounds(254, 322, 86, 20);
		contentPane.add(numcarte);
		numcarte.setColumns(10);
		
		JButton btnPoser = new JButton("Poser Carte");
		btnPoser.setBounds(254, 403, 109, 23);
		contentPane.add(btnPoser);
		
		Listener_Plateau lp=new Listener_Plateau();
		btnPoser.addMouseListener(lp);
		
	}
	
	//m�thode pour afficher les cartes
	//avec une redimension pour les adapter � la taille du JLabel
	public void remplir_image(JLabel cont,String image_path) {		
		ImageIcon image=new ImageIcon(image_path);
		Image temp=image.getImage();
		temp=temp.getScaledInstance(90,150,java.awt.Image.SCALE_SMOOTH);
		cont.setText(null);
		image=new ImageIcon(temp);
		cont.setIcon(image);		
	}
}
