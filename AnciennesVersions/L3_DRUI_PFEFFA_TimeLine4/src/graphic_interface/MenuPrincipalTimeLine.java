package graphic_interface;


import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import game_engine.Joueur;

import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JLabel;


public class MenuPrincipalTimeLine extends JFrame {
	private JTextField nameplayer_1;
	private JTextField nameplayer_2;
	private JTextField ageplayer_1;
	private JTextField ageplayer_2;
	private JTextField nameplayer_5;
	private JTextField nameplayer_6;
	private JTextField ageplayer_5;
	private JTextField ageplayer_6;
	private JTextField nameplayer_3;
	private JTextField nameplayer_4;
	private JTextField ageplayer_3;
	private JTextField ageplayer_4;
	private JTextField nameplayer_7;
	private JTextField nameplayer_8;
	private JTextField ageplayer_7;
	private JTextField ageplayer_8;

	/**
	 * Launch the application. A EFFACER C'EST JUSTE POUR TESTER
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipalTimeLine frame = new MenuPrincipalTimeLine();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuPrincipalTimeLine() {
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//op�ration sur la fen�tre en elle m�me
		this.setTitle("Menu Principal TimeLine");		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 481, 278);
		this.setLocation((screen.width-this.getSize().width)/2, (screen.height-this.getSize().height)/2);
		getContentPane().setLayout(null);

		JButton btnJouer = new JButton("Jouer !");
		btnJouer.setBounds(191, 189, 89, 23);
		getContentPane().add(btnJouer);

		nameplayer_1 = new JTextField();
		nameplayer_1.setBounds(50, 55, 86, 20);
		getContentPane().add(nameplayer_1);
		nameplayer_1.setColumns(10);

		nameplayer_2 = new JTextField();
		nameplayer_2.setBounds(50, 86, 86, 20);
		getContentPane().add(nameplayer_2);
		nameplayer_2.setColumns(10);

		ageplayer_1 = new JTextField();
		ageplayer_1.setBounds(146, 55, 86, 20);
		getContentPane().add(ageplayer_1);
		ageplayer_1.setColumns(10);

		ageplayer_2 = new JTextField();
		ageplayer_2.setBounds(146, 86, 86, 20);
		getContentPane().add(ageplayer_2);
		ageplayer_2.setColumns(10);

		nameplayer_5 = new JTextField();
		nameplayer_5.setBounds(242, 55, 86, 20);
		getContentPane().add(nameplayer_5);
		nameplayer_5.setColumns(10);

		nameplayer_6 = new JTextField();
		nameplayer_6.setBounds(242, 86, 86, 20);
		getContentPane().add(nameplayer_6);
		nameplayer_6.setColumns(10);

		ageplayer_5 = new JTextField();
		ageplayer_5.setBounds(338, 55, 86, 20);
		getContentPane().add(ageplayer_5);
		ageplayer_5.setColumns(10);

		ageplayer_6 = new JTextField();
		ageplayer_6.setBounds(338, 86, 86, 20);
		getContentPane().add(ageplayer_6);
		ageplayer_6.setColumns(10);

		nameplayer_3 = new JTextField();
		nameplayer_3.setBounds(50, 117, 86, 20);
		getContentPane().add(nameplayer_3);
		nameplayer_3.setColumns(10);

		nameplayer_4 = new JTextField();
		nameplayer_4.setBounds(50, 148, 86, 20);
		getContentPane().add(nameplayer_4);
		nameplayer_4.setColumns(10);

		ageplayer_3 = new JTextField();
		ageplayer_3.setBounds(146, 117, 86, 20);
		getContentPane().add(ageplayer_3);
		ageplayer_3.setColumns(10);

		ageplayer_4 = new JTextField();
		ageplayer_4.setBounds(146, 148, 86, 20);
		getContentPane().add(ageplayer_4);
		ageplayer_4.setColumns(10);

		nameplayer_7 = new JTextField();
		nameplayer_7.setBounds(242, 117, 86, 20);
		getContentPane().add(nameplayer_7);
		nameplayer_7.setColumns(10);

		nameplayer_8 = new JTextField();
		nameplayer_8.setBounds(242, 148, 86, 20);
		getContentPane().add(nameplayer_8);
		nameplayer_8.setColumns(10);

		ageplayer_7 = new JTextField();
		ageplayer_7.setBounds(338, 117, 86, 20);
		getContentPane().add(ageplayer_7);
		ageplayer_7.setColumns(10);

		ageplayer_8 = new JTextField();
		ageplayer_8.setBounds(338, 148, 86, 20);
		getContentPane().add(ageplayer_8);
		ageplayer_8.setColumns(10);

		JLabel lblAgePj = new JLabel("Age PJ 1-4");
		lblAgePj.setBounds(146, 30, 86, 14);
		getContentPane().add(lblAgePj);

		JLabel lblNomPj = new JLabel("Nom PJ 1-4");
		lblNomPj.setBounds(50, 30, 86, 14);
		getContentPane().add(lblNomPj);

		JLabel lblNomPj_1 = new JLabel("Nom PJ 5-8");
		lblNomPj_1.setBounds(242, 30, 86, 14);
		getContentPane().add(lblNomPj_1);

		JLabel lblAgePj_1 = new JLabel("Age PJ 5-8");
		lblAgePj_1.setBounds(338, 30, 86, 14);
		getContentPane().add(lblAgePj_1);
		
		Listener_Menu l=new Listener_Menu();
		btnJouer.addMouseListener(l);
	}

	public ArrayList<Joueur> liste_input() {
		ArrayList<Joueur> res=new ArrayList<Joueur>();
		if(!nameplayer_1.getText().equals("")) {
			Joueur j1=new Joueur(nameplayer_1.getText(),ageplayer_1.getText());
			res.add(j1);
		}
		if(!nameplayer_2.getText().equals("")) {
			Joueur j2=new Joueur(nameplayer_2.getText(),ageplayer_2.getText());
			res.add(j2);
		}
		if(!nameplayer_3.getText().equals("")) {
			Joueur j3=new Joueur(nameplayer_3.getText(),ageplayer_3.getText());
			res.add(j3);
		}
		if(!nameplayer_4.getText().equals("")) {
			Joueur j4=new Joueur(nameplayer_4.getText(),ageplayer_4.getText());
			res.add(j4);
		}
		if(!nameplayer_5.getText().equals("")) {
			Joueur j5=new Joueur(nameplayer_5.getText(),ageplayer_5.getText());
			res.add(j5);
		}
		if(!nameplayer_6.getText().equals("")) {
			Joueur j6=new Joueur(nameplayer_6.getText(),ageplayer_6.getText());
			res.add(j6);
		}
		if(!nameplayer_7.getText().equals("")) {
			Joueur j7=new Joueur(nameplayer_7.getText(),ageplayer_7.getText());
			res.add(j7);
		}
		if(!nameplayer_8.getText().equals("")) {
			Joueur j8=new Joueur(nameplayer_8.getText(),ageplayer_8.getText());
			res.add(j8);
		}
		return res;
	}


}
