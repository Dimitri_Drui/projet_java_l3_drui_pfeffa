package graphic_interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class MenuPrincipalCardLine extends JFrame {

	private JPanel contentPane;
	private JTextField nameplayer_1;
	private JTextField nameplayer_2;
	private JTextField ageplayer_1;
	private JTextField ageplayer_2;
	private JTextField nameplayer_5;
	private JTextField nameplayer_6;
	private JTextField ageplayer_5;
	private JTextField ageplayer_6;
	private JTextField nameplayer_3;
	private JTextField nameplayer_4;
	private JTextField ageplayer_3;
	private JTextField ageplayer_4;
	private JTextField nameplayer_7;
	private JTextField nameplayer_8;
	private JTextField ageplayer_7;
	private JTextField ageplayer_8;
	private JRadioButton rdbtnSuperficie;
	private JRadioButton rdbtnPopulation;
	private JRadioButton rdbtnPollution;
	private JRadioButton rdbtnPib;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipalCardLine frame = new MenuPrincipalCardLine();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuPrincipalCardLine() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setTitle("Menu Principal Cardline");
		
		JButton btnJouer = new JButton("Jouer !");
		btnJouer.setBounds(183, 281, 89, 23);
		contentPane.add(btnJouer);
		
		nameplayer_1 = new JTextField();
		nameplayer_1.setBounds(50, 55, 86, 20);
		getContentPane().add(nameplayer_1);
		nameplayer_1.setColumns(10);
		
		nameplayer_2 = new JTextField();
		nameplayer_2.setBounds(50, 86, 86, 20);
		getContentPane().add(nameplayer_2);
		nameplayer_2.setColumns(10);
		
		ageplayer_1 = new JTextField();
		ageplayer_1.setBounds(146, 55, 86, 20);
		getContentPane().add(ageplayer_1);
		ageplayer_1.setColumns(10);
		
		ageplayer_2 = new JTextField();
		ageplayer_2.setBounds(146, 86, 86, 20);
		getContentPane().add(ageplayer_2);
		ageplayer_2.setColumns(10);
		
		nameplayer_5 = new JTextField();
		nameplayer_5.setBounds(242, 55, 86, 20);
		getContentPane().add(nameplayer_5);
		nameplayer_5.setColumns(10);
		
		nameplayer_6 = new JTextField();
		nameplayer_6.setBounds(242, 86, 86, 20);
		getContentPane().add(nameplayer_6);
		nameplayer_6.setColumns(10);
		
		ageplayer_5 = new JTextField();
		ageplayer_5.setBounds(338, 55, 86, 20);
		getContentPane().add(ageplayer_5);
		ageplayer_5.setColumns(10);
		
		ageplayer_6 = new JTextField();
		ageplayer_6.setBounds(338, 86, 86, 20);
		getContentPane().add(ageplayer_6);
		ageplayer_6.setColumns(10);
		
		nameplayer_3 = new JTextField();
		nameplayer_3.setBounds(50, 117, 86, 20);
		getContentPane().add(nameplayer_3);
		nameplayer_3.setColumns(10);
		
		nameplayer_4 = new JTextField();
		nameplayer_4.setBounds(50, 148, 86, 20);
		getContentPane().add(nameplayer_4);
		nameplayer_4.setColumns(10);
		
		ageplayer_3 = new JTextField();
		ageplayer_3.setBounds(146, 117, 86, 20);
		getContentPane().add(ageplayer_3);
		ageplayer_3.setColumns(10);
		
		ageplayer_4 = new JTextField();
		ageplayer_4.setBounds(146, 148, 86, 20);
		getContentPane().add(ageplayer_4);
		ageplayer_4.setColumns(10);
		
		nameplayer_7 = new JTextField();
		nameplayer_7.setBounds(242, 117, 86, 20);
		getContentPane().add(nameplayer_7);
		nameplayer_7.setColumns(10);
		
		nameplayer_8 = new JTextField();
		nameplayer_8.setBounds(242, 148, 86, 20);
		getContentPane().add(nameplayer_8);
		nameplayer_8.setColumns(10);
		
		ageplayer_7 = new JTextField();
		ageplayer_7.setBounds(338, 117, 86, 20);
		getContentPane().add(ageplayer_7);
		ageplayer_7.setColumns(10);
		
		ageplayer_8 = new JTextField();
		ageplayer_8.setBounds(338, 148, 86, 20);
		getContentPane().add(ageplayer_8);
		ageplayer_8.setColumns(10);
		
		JLabel lblAgePj = new JLabel("Age PJ 1-4");
		lblAgePj.setBounds(146, 30, 86, 14);
		getContentPane().add(lblAgePj);
		
		JLabel lblNomPj = new JLabel("Nom PJ 1-4");
		lblNomPj.setBounds(50, 30, 86, 14);
		getContentPane().add(lblNomPj);
		
		JLabel lblNomPj_1 = new JLabel("Nom PJ 5-8");
		lblNomPj_1.setBounds(242, 30, 86, 14);
		getContentPane().add(lblNomPj_1);
		
		JLabel lblAgePj_1 = new JLabel("Age PJ 5-8");
		lblAgePj_1.setBounds(338, 30, 86, 14);
		getContentPane().add(lblAgePj_1);
		
		rdbtnSuperficie = new JRadioButton("superficie");
		rdbtnSuperficie.setBounds(50, 220, 86, 23);
		contentPane.add(rdbtnSuperficie);
		
		rdbtnPopulation = new JRadioButton("population");
		rdbtnPopulation.setBounds(146, 220, 89, 23);
		contentPane.add(rdbtnPopulation);
		
		rdbtnPollution = new JRadioButton("pollution");
		rdbtnPollution.setBounds(242, 220, 89, 23);
		contentPane.add(rdbtnPollution);
		
		rdbtnPib = new JRadioButton("pib");
		rdbtnPib.setBounds(338, 220, 55, 23);
		contentPane.add(rdbtnPib);
	}
}
