package game_engine;



public class Carte {
	
	private Carte gauche=null;
	private Carte droite=null;
	
	public Carte() {
		
	}
	
	public Carte getGauche() {
		return gauche;
	}
	public void setGauche(Carte gauche) {
		this.gauche = gauche;
	}
	public Carte getDroite() {
		return droite;
	}
	public void setDroite(Carte droite) {
		this.droite = droite;
	}
	
	public String toStringRecto_t() {
		return ((TimeLine)this).toStringRecto();
	}
	
	public String toStringVerso_t() {
		return ((TimeLine)this).toStringVerso();
	}
	
	public String toStringRecto_c() {
		return ((CardLine)this).toStringRecto();
	}
	
	public String toStringVerso_c() {
		return ((CardLine)this).toStringVerso();
	}
	
	public boolean test_date_gauche(int comp){
		if(((TimeLine)this).getDate()<comp) {
			return true;
		} return false;
	}
	
	public boolean test_date_droite(int comp) {
		if(((TimeLine)this).getDate()>comp) {
			if(this.droite==null) {
				return true;
			}
			if(((TimeLine)this.droite).getDate()>comp) {
				
			}
			
		} return false;
	}
	
}
