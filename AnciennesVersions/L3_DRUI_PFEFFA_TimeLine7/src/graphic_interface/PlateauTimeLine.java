package graphic_interface;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import game_engine.*;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JButton;


public class PlateauTimeLine extends JFrame {

	private JPanel contentPane;
	private boolean win=false;
	private Carte to_place;
	private Carte current_card;
	private String gd;
	private int nbc,nbc2;
	JButton hand_1 = new JButton("");
	JButton hand_2 = new JButton("");
	JButton hand_3 = new JButton("");
	JButton hand_4 = new JButton("");
	JButton hand_5 = new JButton("");
	JButton played_left = new JButton("GAUCHE");
	JButton played_center = new JButton("");
	JButton played_right = new JButton("DROITE");
	
	public PlateauTimeLine(ArrayList<Joueur> l) throws Exception {
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1000, 500);
		this.setTitle("Plateau de Jeu TimeLine");
		this.setLocation((screen.width-this.getSize().width)/2, (screen.height-this.getSize().height)/2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCartesJoues = new JLabel("Cartes jou\u00E9es :");
		lblCartesJoues.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCartesJoues.setBounds(10, 11, 130, 33);
		contentPane.add(lblCartesJoues);

		JLabel lblVotreMain = new JLabel("Votre main :");
		lblVotreMain.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVotreMain.setBounds(254, 365, 103, 14);
		contentPane.add(lblVotreMain);

		JLabel lblNomDuJoueur = new JLabel("Nom du Joueur :");
		lblNomDuJoueur.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNomDuJoueur.setBounds(10, 365, 145, 14);
		contentPane.add(lblNomDuJoueur);

		JLabel aff_nom = new JLabel("");
		aff_nom.setFont(new Font("Tahoma", Font.PLAIN, 18));
		aff_nom.setBounds(10, 390, 130, 22);
		contentPane.add(aff_nom);

		
		hand_1.setBounds(387, 295, 89, 156);
		contentPane.add(hand_1);

		hand_2.setBounds(487, 295, 89, 156);
		contentPane.add(hand_2);

		hand_3.setBounds(586, 295, 89, 156);
		contentPane.add(hand_3);

		hand_4.setBounds(685, 295, 89, 156);
		contentPane.add(hand_4);
		
		hand_5.setBounds(784, 295, 89, 156);
		contentPane.add(hand_5);

		
		played_left.setBounds(10, 55, 89, 156);
		contentPane.add(played_left);

		
		played_center.setBounds(110, 55, 89, 156);
		contentPane.add(played_center);

		
		played_right.setBounds(209, 55, 89, 156);
		contentPane.add(played_right);
		
		JLabel lblScore = new JLabel("Score :");
		lblScore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblScore.setBounds(10, 415, 62, 14);
		contentPane.add(lblScore);
		
		JLabel aff_score = new JLabel("");
		aff_score.setFont(new Font("Tahoma", Font.PLAIN, 18));
		aff_score.setBounds(10, 440, 62, 22);
		contentPane.add(aff_score);

		//gestion du jeu ci-dessous
		Partie p=new Partie("timeline",l);
		p.premier_joueur();
		//while(win==false) {
			remplir_image(played_center,chemin_v(((TimeLine)p.played_cards.get(0)).getImage()));

			
			// ce for N'AVANCE PAS ET C'EST TRES CHIANT
			for(Joueur j : p.liste_joueurs) {
				//on affiche les cartes du joueur
				remplir_image(hand_1,chemin_r(((TimeLine)j.getMain().get(0)).getImage()));
				remplir_image(hand_2,chemin_r(((TimeLine)j.getMain().get(1)).getImage()));
				remplir_image(hand_3,chemin_r(((TimeLine)j.getMain().get(2)).getImage()));
				remplir_image(hand_4,chemin_r(((TimeLine)j.getMain().get(3)).getImage()));
				remplir_image(hand_5,chemin_r(((TimeLine)j.getMain().get(4)).getImage()));
				aff_nom.setText(j.getPseudo());
				aff_score.setText(String.valueOf(j.getScore()));
			
				hand_1.addMouseListener(new MouseListener() {
					@Override
					public void mouseClicked(MouseEvent e) {
						to_place=(TimeLine)j.getMain().get(0);
						
						played_left.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_gauche(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}							
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}
						});
						
						played_right.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_droite(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}					
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}							
						});
					}
					@Override
					public void mousePressed(MouseEvent e) {}
					@Override
					public void mouseReleased(MouseEvent e) {}
					@Override
					public void mouseEntered(MouseEvent e) {}
					@Override
					public void mouseExited(MouseEvent e) {}
				});
				
				hand_2.addMouseListener(new MouseListener() {
					@Override
					public void mouseClicked(MouseEvent e) {
						to_place=(TimeLine)j.getMain().get(1);
						played_left.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_gauche(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}							
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}
						});
						
						played_right.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_droite(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}					
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}							
						});
					}
					@Override
					public void mousePressed(MouseEvent e) {}
					@Override
					public void mouseReleased(MouseEvent e) {}
					@Override
					public void mouseEntered(MouseEvent e) {}
					@Override
					public void mouseExited(MouseEvent e) {}
				});
				
				hand_3.addMouseListener(new MouseListener() {
					@Override
					public void mouseClicked(MouseEvent e) {
						to_place=(TimeLine)j.getMain().get(2);
						played_left.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_gauche(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}							
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}
						});
						
						played_right.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_droite(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}					
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}							
						});
					}
					@Override
					public void mousePressed(MouseEvent e) {}
					@Override
					public void mouseReleased(MouseEvent e) {}
					@Override
					public void mouseEntered(MouseEvent e) {}
					@Override
					public void mouseExited(MouseEvent e) {}
				});
				
				hand_4.addMouseListener(new MouseListener() {
					@Override
					public void mouseClicked(MouseEvent e) {
						to_place=(TimeLine)j.getMain().get(3);
						played_left.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_gauche(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}							
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}
						});
						
						played_right.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_droite(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}					
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}							
						});
					}
					@Override
					public void mousePressed(MouseEvent e) {}
					@Override
					public void mouseReleased(MouseEvent e) {}
					@Override
					public void mouseEntered(MouseEvent e) {}
					@Override
					public void mouseExited(MouseEvent e) {}
				});
				
				hand_5.addMouseListener(new MouseListener() {
					@Override
					public void mouseClicked(MouseEvent e) {
						to_place=(TimeLine)j.getMain().get(4);
						played_left.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_gauche(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}							
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));									
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}
						});
						
						played_right.addMouseListener(new MouseListener() {
							@Override
							public void mouseClicked(MouseEvent e) {
								current_card=(TimeLine)p.played_cards.get(0);
								
								if(current_card.test_date_droite(((TimeLine)to_place).getDate())) {
									if(p.played_cards.indexOf(current_card)-1<0) {
										p.played_cards.add(0, to_place);
									} else {
										p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
									}					
									j.poseUneCarte(to_place);						
									p.discard_pile.add(to_place);
									remplir_image(played_center,chemin_v(((TimeLine) to_place).getImage()));
									aff_score.setText(String.valueOf(j.getScore()+1));
								} else {
									j.poseUneCarte(to_place);
									p.discard_pile.add(to_place);
									p.pick_one_card(j);
								}
							}
							@Override
							public void mousePressed(MouseEvent e) {}
							@Override
							public void mouseReleased(MouseEvent e) {}
							@Override
							public void mouseEntered(MouseEvent e) {}
							@Override
							public void mouseExited(MouseEvent e) {}							
						});
					}
					@Override
					public void mousePressed(MouseEvent e) {}
					@Override
					public void mouseReleased(MouseEvent e) {}
					@Override
					public void mouseEntered(MouseEvent e) {}
					@Override
					public void mouseExited(MouseEvent e) {}
				});
			} // fin du for
			// � changer uniquement quand le jeu sera fonctionnel
			win=true;
		//} //fin du while
	}

	//m�thode pour afficher les cartes
	//avec une redimension pour les adapter � la taille du JLabel
	public void remplir_image(JButton cont,String image_path) {		
		ImageIcon image=new ImageIcon(image_path);
		Image temp=image.getImage();
		temp=temp.getScaledInstance(90,150,java.awt.Image.SCALE_SMOOTH);
		cont.setText(null);
		image=new ImageIcon(temp);
		cont.setIcon(image);		
	}

	public String chemin_r(String s) {
		return ".\\data\\timeline\\cards\\"+s+".jpeg";
	}

	public String chemin_v(String s) {
		return ".\\data\\timeline\\cards\\"+s+"_date.jpeg";
	}
}
