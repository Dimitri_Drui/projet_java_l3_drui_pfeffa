package cards_comp;

import java.util.Comparator;

import game_engine.CardLine;
import game_engine.Carte;

public class CardComparatorPol implements Comparator<Carte> {
	
	@Override
	public int compare(Carte o1, Carte o2) {
		Double i=new Double(((CardLine)o1).getPollution());
		Double j=new Double(((CardLine)o2).getPollution());
		return i.compareTo(j);
	}

}
