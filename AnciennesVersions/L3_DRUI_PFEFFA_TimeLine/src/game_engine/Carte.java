package game_engine;
// Cardline � implementer, faire en sorte que les cartes soient parametrables avec les objets
// creer deux objets en fonction des cartes utilis�s
// differenci� la classe PartieTimeline et creer un classe PartieCardline

public class Carte {
	private int date_creation;
	private String invention;
	private String theme;
	private Carte gauche=null;
	private Carte droite=null;
	
	// s2=Inventions/D�couvertes/Ev�nements historique
	// Monuments/Art&Litt�rature/Musique
	public Carte(String s1,int d,String s2) {		
		invention=s1;
		date_creation=d;
		theme=s2;
	}

	public int getDate_creation() {
		return date_creation;
	}

	public String getInvention() {
		return invention;
	}

	public String getTheme() {
		return theme;
	}
	
	// Gestion de la contrainte "on doit pas voir les dates avant un certain moment"s
	public String toStringRecto() {
		return "[ "+invention+" ; "+theme+" ]";
	}
	
	public String toStringVerso() {
		return "[ "+invention+" ; "+date_creation+" ; "+theme+" ]";
	}
	
	public void setGauche(Carte g) {
		gauche = g;
	}

	public void setDroite(Carte d) {
		droite = d;
	}

	public String aide_place() {
		String res="";
		if(test_place(gauche)&&test_place(droite)) {
			res="Les deux emplacements sont libres";
		} else {
			if(test_place(gauche)) {
				res="La place gauche est libre, la place droite est prise par : \n"+droite.toStringRecto();
			} else {
				if(test_place(droite)) {
					res="La place droite est libre, la place gauche est prise par : \n"+gauche.toStringRecto();
				} else {
					res="Les deux emplacements sont pris par "+gauche.toStringRecto()+"\n et par "+droite.toStringRecto();
				}					
			}
			
		}
		return res;
	}
	
	private boolean test_place(Carte emplacement) {
		if(emplacement==null) {
			return true;
		} return false;
	}
	
	public boolean test_date_gauche(int date_comp) {
		if(date_creation<date_comp) {
			return true;
		} return false;
	}
	
	public boolean test_date_droite(int date_comp) {
		if(date_creation>date_comp) {
			return true;
		}return false;
	}
	
}
