package reader_csv;
import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;

import game_engine.Carte;

public class Reader {

	private ArrayList<Carte> liste_cartes=new ArrayList<Carte>();

	// sÚparateur c'est la ','
	//le constructeur lit et affiche le fichier voulu sans faire de traitement
	public Reader(String file_name) throws FileNotFoundException,IOException, ParseException {
		String line = "";
		BufferedReader br = new BufferedReader(new FileReader(file_name));
		//lit ligne par ligne
		br.readLine();
		while ((line = br.readLine()) != null) {
			String[] current = line.split(";");
			Carte c=new Carte(current[0],toInt(current[1]),current[2]);
			liste_cartes.add(c);
		}
		br.close();
	}

	public int toInt(String s) {
		return new Integer(s);
	}

	public ArrayList<Carte> getListe_cartes() {
		return liste_cartes;
	}

}