package game_engine;

public class TimeLine extends Carte {
	private String invention;
	private int date;
	private String image;
	
	public TimeLine(String in,int d,String im) {
		invention=in;
		date=d;
		image=im;
	}

	public String getInvention() {
		return invention;
	}

	public int getDate() {
		return date;
	}

	public String getImage() {
		return image;
	}
	
	public String toStringRecto() {
		return "[ "+invention+" ; "+image+" ]";
	}
	
	public String toStringVerso() {
		return "[ "+invention+" ; "+String.valueOf(date)+" ; "+image+" ]";
	}
	

}