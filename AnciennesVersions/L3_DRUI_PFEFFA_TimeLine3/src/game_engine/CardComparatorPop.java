package game_engine;

import java.util.Comparator;

public class CardComparatorPop implements Comparator<Carte> {
	
	@Override
	public int compare(Carte o1, Carte o2) {
		Double i=new Double(((CardLine)o1).getPopulation());
		Double j=new Double(((CardLine)o2).getPopulation());
		return i.compareTo(j);
	}

}
