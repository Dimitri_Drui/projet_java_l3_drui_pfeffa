package game_engine;

import java.util.Comparator;

public class CardComparatorPib implements Comparator<Carte> {
	
	@Override
	public int compare(Carte o1, Carte o2) {
		Double i=new Double(((CardLine)o1).getPib());
		Double j=new Double(((CardLine)o2).getPib());
		return i.compareTo(j);
	}

}