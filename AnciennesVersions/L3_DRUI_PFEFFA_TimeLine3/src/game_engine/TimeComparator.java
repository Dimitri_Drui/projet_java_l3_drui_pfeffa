package game_engine;

import java.util.Comparator;

public class TimeComparator implements Comparator<Carte> {

	@Override
	public int compare(Carte o1, Carte o2) {
		Integer i=new Integer(((TimeLine)o1).getDate());
		Integer j=new Integer(((TimeLine)o2).getDate());
		return i.compareTo(j);
	}

}
