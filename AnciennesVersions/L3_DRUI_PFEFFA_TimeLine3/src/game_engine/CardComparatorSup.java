package game_engine;

import java.util.Comparator;

public class CardComparatorSup implements Comparator<Carte> {
	
	@Override
	public int compare(Carte o1, Carte o2) {
		Double i=new Double(((CardLine)o1).getSuperficie());
		Double j=new Double(((CardLine)o2).getSuperficie());
		return i.compareTo(j);
	}

}
