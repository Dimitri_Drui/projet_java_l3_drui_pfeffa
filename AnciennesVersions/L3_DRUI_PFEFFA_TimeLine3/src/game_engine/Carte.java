package game_engine;



public class Carte {
	
	public Carte() {
		
	}
	
	public String toStringRecto_t() {
		return ((TimeLine)this).toStringRecto();
	}
	
	public String toStringVerso_t() {
		return ((TimeLine)this).toStringVerso();
	}
	
	public String toStringRecto_c() {
		return ((CardLine)this).toStringRecto();
	}
	
	public String toStringVerso_c() {
		return ((CardLine)this).toStringVerso();
	}
	
	public boolean test_date_gauche(int comp){
		if(((TimeLine)this).getDate()>comp) {
			return true;
		} return false;
	}
	
	public boolean test_date_droite(int comp) {
		if(((TimeLine)this).getDate()<comp) {
			return true;			
		} return false;
	}
	
	public boolean test_sup_gauche(double comp) {
		if(((CardLine)this).getSuperficie()>comp) {
			return true;
		} return false;
	}
	public boolean test_pop_gauche(double comp) {
		if(((CardLine)this).getPopulation()>comp) {
			return true;
		} return false;
	}
	public boolean test_pib_gauche(double comp) {
		if(((CardLine)this).getPib()>comp) {
			return true;
		} return false;
	}
	public boolean test_pol_gauche(double comp) {
		if(((CardLine)this).getPollution()>comp) {
			return true;
		} return false;
	}
	public boolean test_sup_droite(double comp) {
		if(((CardLine)this).getSuperficie()<comp) {
			return true;
		} return false;
	}
	public boolean test_pop_droite(double comp) {
		if(((CardLine)this).getPopulation()<comp) {
			return true;
		} return false;
	}
	public boolean test_pib_droite(double comp) {
		if(((CardLine)this).getPib()<comp) {
			return true;
		} return false;
	}
	public boolean test_pol_droite(double comp) {
		if(((CardLine)this).getPollution()<comp) {
			return true;
		} return false;
	}
}
