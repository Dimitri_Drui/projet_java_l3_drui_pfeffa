package game_engine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import reader_csv.Reader;

public class Partie {

	private ArrayList<Carte> discard_pile=new ArrayList<Carte>();
	private ArrayList<Carte> paquet=new ArrayList<Carte>();
	private ArrayList<Joueur> liste_joueurs=new ArrayList<Joueur>();
	private ArrayList<Carte> played_cards=new ArrayList<Carte>();

	public Partie(String choix_1,ArrayList<Joueur> l) throws FileNotFoundException, IOException, ParseException, Exception {
		Scanner sc=new Scanner(System.in);
		if(choix_1.equals("timeline")) {
			Reader r=new Reader();
			paquet=r.getListe_cartes();
			//ajout de la liste de joueurs
			liste_joueurs.addAll(l);
			//dire qui joue en premier et l'attribuer en premi�re place de liste
			Joueur first=first_player();
			System.out.println("C'est ton jour de chance "+first.getPseudo()+" ! \n"+"tu joue le premier !");
			update_list(first);

			//m�langer les cartes
			random_shuffle(paquet);
			// distribution des cartes
			card_pick();		

			//on r�v�le la premi�re carte
			played_cards.add(paquet.get(0));
			discard_pile.add(paquet.get(0));
			paquet.remove(0);

			boolean win=false;
			Carte to_place;
			Carte current_card;
			while(win!=true) {
				for(Joueur j : liste_joueurs) {
					System.out.println("C'est le tour de : "+j.getPseudo());
					for(int i=0;i<j.getMain().size();i++) {
						System.out.println(j.getMain().get(i).toStringRecto_t());
					}
					for(int k=0;k<played_cards.size();k++) {
						System.out.println(played_cards.get(k).toStringVerso_t());
					}

					System.out.println("n� de carte � placer ? 0..n");
					int temp=sc.nextInt();
					to_place=j.getMain().get(temp);

					System.out.println("n� de la carte sur la table sur laquelle vous voulez placer ? 0..n");
					int temp2=sc.nextInt();
					current_card=played_cards.get(temp2);

					System.out.println("Gauche ou droite ? g/d");
					String choix=sc.next();

					if(choix.equals("g")) {
						if(current_card.test_date_gauche(((TimeLine)to_place).getDate())) {
							if(played_cards.indexOf(current_card)-1<0) {
								played_cards.add(0, to_place);
							} else {
								played_cards.add(played_cards.indexOf(current_card)-1, to_place);
							}							
							System.out.println("La carte a �t� bien plac�e !");							
							j.poseUneCarte(to_place);						
							discard_pile.add(to_place);
						} else {
							System.out.println("La date n'est pas la bonne.");
							j.poseUneCarte(to_place);
							discard_pile.add(to_place);
							pick_one_card(j);
							j.getLastCard().toStringRecto_t();
						}

					}
					if(choix.equals("d")) {
						if(current_card.test_date_droite(((TimeLine)to_place).getDate())) {							
							played_cards.add(played_cards.indexOf(current_card)+1, to_place);
							System.out.println("La carte a �t� bien plac�e !");							
							j.poseUneCarte(to_place);
							discard_pile.add(to_place);
						} else {
							System.out.println("La date n'est pas la bonne.");
							j.poseUneCarte(to_place);
							pick_one_card(j);
							j.getLastCard().toStringRecto_t();
						}
					}
				}
				sort_played_card_timeline();
				reset_paquet_if_needed();
				win=win_condition();
			}
			sc.close();
		}
		if(choix_1.equals("cardline")) {
			Reader r=new Reader(2);
			//choix du crit�re pour la partie ce qui va en gros permettre de trier les cartes
			System.out.println("choisissez le crit�re selon lequel la partie va se jouer : ");
			String critere=sc.next();
			paquet=r.getListe_cartes();
			//ajout de la liste de joueurs
			liste_joueurs.addAll(l);
			//dire qui joue en premier et l'attribuer en premi�re place de liste
			Joueur first=first_player();
			System.out.println("C'est ton jour de chance "+first.getPseudo()+" ! \n"+"tu joue le premier !");
			update_list(first);

			//m�langer les cartes
			random_shuffle(paquet);
			// distribution des cartes
			card_pick();		

			//on r�v�le la premi�re carte
			played_cards.add(paquet.get(0));
			discard_pile.add(paquet.get(0));
			paquet.remove(0);

			boolean win=false;
			Carte to_place;
			Carte current_card;while(win!=true) {
				for(Joueur j : liste_joueurs) {
					System.out.println("C'est le tour de : "+j.getPseudo());
					for(int i=0;i<j.getMain().size();i++) {
						System.out.println(j.getMain().get(i).toStringRecto_c());
					}
					for(int k=0;k<played_cards.size();k++) {
						System.out.println(played_cards.get(k).toStringVerso_c());
					}

					System.out.println("n� de carte � placer ? 0..n");
					int temp=sc.nextInt();
					to_place=j.getMain().get(temp);

					System.out.println("n� de la carte sur la table sur laquelle vous voulez placer ? 0..n");
					int temp2=sc.nextInt();
					current_card=played_cards.get(temp2);

					System.out.println("Gauche ou droite ? g/d");
					String choix=sc.next();

					if(choix.equals("g")) {
						switch(critere) {
						case "superficie":
							if(current_card.test_sup_gauche(((CardLine)to_place).getSuperficie())) {
								if(played_cards.indexOf(current_card)-1<0) {
									played_cards.add(0, to_place);
								} else {
									played_cards.add(played_cards.indexOf(current_card)-1, to_place);
								}							
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);						
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						case "population":
							if(current_card.test_pop_gauche(((CardLine)to_place).getSuperficie())) {
								if(played_cards.indexOf(current_card)-1<0) {
									played_cards.add(0, to_place);
								} else {
									played_cards.add(played_cards.indexOf(current_card)-1, to_place);
								}							
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);						
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						case "pollution":
							if(current_card.test_pol_gauche(((CardLine)to_place).getSuperficie())) {
								if(played_cards.indexOf(current_card)-1<0) {
									played_cards.add(0, to_place);
								} else {
									played_cards.add(played_cards.indexOf(current_card)-1, to_place);
								}							
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);						
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						case "pib":
							if(current_card.test_pib_gauche(((CardLine)to_place).getSuperficie())) {
								if(played_cards.indexOf(current_card)-1<0) {
									played_cards.add(0, to_place);
								} else {
									played_cards.add(played_cards.indexOf(current_card)-1, to_place);
								}							
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);						
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						default:
							break;
						}
						

					}
					if(choix.equals("d")) {
						switch(critere) {
						case "superficie":
							if(current_card.test_sup_droite(((CardLine)to_place).getSuperficie())) {							
								played_cards.add(played_cards.indexOf(current_card)+1, to_place);
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						case "population":
							if(current_card.test_pop_droite(((CardLine)to_place).getSuperficie())) {							
								played_cards.add(played_cards.indexOf(current_card)+1, to_place);
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						case "pollution":
							if(current_card.test_pol_droite(((CardLine)to_place).getSuperficie())) {							
								played_cards.add(played_cards.indexOf(current_card)+1, to_place);
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						case "pib":
							if(current_card.test_pib_droite(((CardLine)to_place).getSuperficie())) {							
								played_cards.add(played_cards.indexOf(current_card)+1, to_place);
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);
								discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								pick_one_card(j);
								j.getLastCard().toStringRecto_c();
							}
							break;
						default:
							break;
						}
						
						
					}
				}
				sort_played_card_cardline(critere);
				reset_paquet_if_needed();
				win=win_condition();
			}
			sc.close();
		}
	}

	// d�termine si la win conditione est atteinte et donne le gagnant s'il y en a un
	private boolean win_condition() {
		for(Joueur j : liste_joueurs) {
			if(j.aGagne()) {
				System.out.println("Le joueur : "+j.toString()+" a gagn� !" );
				return true;
			}
		} return false;
	}

	// d�termine qui doit jouer le premier en fonction de son age
	private Joueur first_player() {
		int min=liste_joueurs.get(0).getAge();
		Joueur res=liste_joueurs.get(0);
		for(Joueur j : liste_joueurs) {
			if(min>j.getAge()) {
				min=j.getAge();
				res=j;
			}
		}
		return res;		
	}

	private int nb_joueurs() {
		return liste_joueurs.size();
	}

	// d�termine le nombre de cartes � piocher en fonction du nombre de joueurs pr�sents
	private int nb_to_pick() throws Exception {
		if(nb_joueurs()<=3) {
			return 6;
		} else {
			if(nb_joueurs()<=5) {
				return 5;
			} else {
				if(nb_joueurs()<=8) {
					return 4;
				}
			}
		}
		throw new Exception("Le nombre de cartes d�passe l'entendement.");		
	}

	// faire piocher une carte du paquet au joueur
	private void pick_one_card(Joueur j) {
		j.getMain().add(paquet.get(0));
		paquet.remove(0);
	}

	// m�lange les cartes
	private void random_shuffle(ArrayList<Carte> l_c) {
		Collections.shuffle(l_c);
	}

	// fait piocher les cartes � chaque joueurs
	// pb les joueurs ont TOUS les m�mes cartes
	private void card_pick() throws Exception {		
		for(Joueur j : liste_joueurs) {
			for(int i=0;i<nb_to_pick();i++) {
				j.ajouter_carte_main(paquet.get(i));
				paquet.remove(i);
			}
		}
	}

	// met � jour la liste des joueurs pour les d�caler en fonction du premier
	private void update_list(Joueur first) {
		int ref=0;
		for(int i=0;i<liste_joueurs.size();i++) {
			if(liste_joueurs.get(i).equals(first)) {
				ref=i;
			}
		}
		Collections.swap(liste_joueurs, ref, ref-1);
	}

	// si le paquet est vide >> prendre la d�fausse shuffle+
	private void reset_paquet_if_needed() {
		if(paquet.isEmpty()) {
			random_shuffle(discard_pile);
			paquet.addAll(discard_pile);
		}
	}

	//trie les cartes jou�es pour timeline
	private void sort_played_card_timeline() {
		Comparator<Carte> c=new TimeComparator();
		played_cards.sort(c);
	}
	
	//trie les cartes jou�es pour cardline selon crit�re choisi
	private void sort_played_card_cardline(String s) {
		if(s.equals("pib")) {
			Comparator<Carte> c=new CardComparatorPib();
			played_cards.sort(c);
		}
		if(s.equals("pollution")) {
			Comparator<Carte> c=new CardComparatorPol();
			played_cards.sort(c);
		}
		if(s.equals("population")) {
			Comparator<Carte> c=new CardComparatorPop();
			played_cards.sort(c);
		}		
		if(s.equals("superficie")) {
			Comparator<Carte> c=new CardComparatorSup();
			played_cards.sort(c);
		}		
	}
	
	
	


}