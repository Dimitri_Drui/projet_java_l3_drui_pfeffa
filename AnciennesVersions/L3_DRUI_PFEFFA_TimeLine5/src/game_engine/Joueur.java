package game_engine;

import java.util.ArrayList;

public class Joueur {
	private String pseudo;
	private int age;
	private ArrayList<Carte> main=new ArrayList<Carte>();

	public Joueur(String pse, int a) {
		pseudo = pse;
		age = a;
	}
	
	public Joueur(String pse, String a) {
		pseudo=pse;
		age=Integer.parseInt(a);
	}

	public boolean aGagne() {
		if(main.size()==0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void ajouter_carte_main(Carte c) {
		main.add(c);
	}

	public void poseUneCarte(Carte carte) {
		main.remove(carte);
	}

	public String getPseudo() {
		return pseudo;
	}

	public int getAge() {
		return age;
	}

	public int getNbCartes() {
		return main.size();
	}

	public ArrayList<Carte> getMain() {
		return main;
	}

	@Override
	public String toString() {
		return "Joueur [pseudo=" + pseudo + ", age=" + age + "]";
	}
	
	
	
}
