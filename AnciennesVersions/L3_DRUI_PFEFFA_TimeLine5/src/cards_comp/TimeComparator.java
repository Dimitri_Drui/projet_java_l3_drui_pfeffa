package cards_comp;

import java.util.Comparator;

import game_engine.Carte;
import game_engine.TimeLine;

public class TimeComparator implements Comparator<Carte> {

	@Override
	public int compare(Carte o1, Carte o2) {
		Integer i=new Integer(((TimeLine)o1).getDate());
		Integer j=new Integer(((TimeLine)o2).getDate());
		return i.compareTo(j);
	}

}
