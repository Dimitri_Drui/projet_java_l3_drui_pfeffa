package graphic_interface;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import game_engine.*;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JTextField;
import javax.swing.JButton;


public class PlateauTimeLine extends JFrame {

	private JPanel contentPane;
	private JTextField place;
	private JTextField numcarte;
	private JTextField numcarte2;
	private boolean win=false;
	private Carte to_place;
	private Carte current_card;
	private String gd;
	private int nbc,nbc2;

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public PlateauTimeLine(ArrayList<Joueur> l) throws Exception {
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1000, 500);
		this.setTitle("Plateau de Jeu TimeLine");
		this.setLocation((screen.width-this.getSize().width)/2, (screen.height-this.getSize().height)/2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCartesJoues = new JLabel("Cartes jou\u00E9es :");
		lblCartesJoues.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCartesJoues.setBounds(10, 11, 130, 33);
		contentPane.add(lblCartesJoues);

		JLabel lblVotreMain = new JLabel("Votre main :");
		lblVotreMain.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVotreMain.setBounds(254, 437, 103, 14);
		contentPane.add(lblVotreMain);

		JLabel lblNomDuJoueur = new JLabel("Nom du Joueur :");
		lblNomDuJoueur.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNomDuJoueur.setBounds(10, 404, 145, 14);
		contentPane.add(lblNomDuJoueur);

		JLabel contimghand_1 = new JLabel("");
		contimghand_1.setBounds(387, 322, 90, 150);
		contentPane.add(contimghand_1);

		JLabel contimghand_2 = new JLabel("");
		contimghand_2.setBounds(484, 322, 90, 150);
		contentPane.add(contimghand_2);

		JLabel contimghand_3 = new JLabel("");
		contimghand_3.setBounds(582, 322, 90, 150);
		contentPane.add(contimghand_3);

		JLabel contimghand_4 = new JLabel("");
		contimghand_4.setBounds(680, 322, 90, 150);
		contentPane.add(contimghand_4);

		JLabel contimghand_5 = new JLabel("");
		contimghand_5.setBounds(778, 322, 90, 150);
		contentPane.add(contimghand_5);

		JLabel lblNewLabel = new JLabel("G/D ?");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(254, 353, 54, 14);
		contentPane.add(lblNewLabel);

		place = new JTextField();
		place.setBounds(254, 378, 86, 20);
		contentPane.add(place);
		place.setColumns(10);

		JLabel lblNCarte = new JLabel("N\u00B0 Carte Main ?");
		lblNCarte.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNCarte.setBounds(254, 297, 130, 14);
		contentPane.add(lblNCarte);

		numcarte = new JTextField();
		numcarte.setBounds(254, 322, 86, 20);
		contentPane.add(numcarte);
		numcarte.setColumns(10);

		JButton btnPoser = new JButton("Poser Carte");
		btnPoser.setBounds(254, 403, 109, 23);
		contentPane.add(btnPoser);

		JLabel contimgplay_1 = new JLabel("");
		contimgplay_1.setBounds(50, 55, 90, 150);
		contentPane.add(contimgplay_1);

		JLabel contimgplay_2 = new JLabel("");
		contimgplay_2.setBounds(150, 55, 90, 150);
		contentPane.add(contimgplay_2);

		JLabel contimgplay_3 = new JLabel("");
		contimgplay_3.setBounds(218, 55, 90, 150);
		contentPane.add(contimgplay_3);

		JLabel aff_nom = new JLabel("");
		aff_nom.setFont(new Font("Tahoma", Font.PLAIN, 18));
		aff_nom.setBounds(10, 440, 130, 14);
		contentPane.add(aff_nom);

		JLabel lblNCarteBoard = new JLabel("N\u00B0 Carte Board ?");
		lblNCarteBoard.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNCarteBoard.setBounds(254, 238, 139, 20);
		contentPane.add(lblNCarteBoard);

		numcarte2 = new JTextField();
		numcarte2.setBounds(254, 269, 86, 20);
		contentPane.add(numcarte2);
		numcarte2.setColumns(10);
		
		// pb la m�thode test_date_gauche et droite font de la merde

		//gestion du jeu ci-dessous
		Partie p=new Partie("timeline",l);
		p.premier_joueur();
		while(win==false) {
			remplir_image(contimgplay_2,chemin_v(((TimeLine)p.played_cards.get(0)).getImage()));
			//remplir_image(contimgplay_2,((TimeLine)p.getPlayed_cards().get(1)).getImage());
			//remplir_image(contimgplay_3,((TimeLine)p.getPlayed_cards().get(2)).getImage());
			
			for(Joueur j : p.liste_joueurs) {
				//on affiche les cartes du joueur
				remplir_image(contimghand_1,chemin_r(((TimeLine)j.getMain().get(0)).getImage()));
				remplir_image(contimghand_2,chemin_r(((TimeLine)j.getMain().get(1)).getImage()));
				remplir_image(contimghand_3,chemin_r(((TimeLine)j.getMain().get(2)).getImage()));
				remplir_image(contimghand_4,chemin_r(((TimeLine)j.getMain().get(3)).getImage()));
				remplir_image(contimghand_5,chemin_r(((TimeLine)j.getMain().get(4)).getImage()));
				aff_nom.setText(j.getPseudo());

				//on r�cup�re le choix du joueur
				
				btnPoser.addMouseListener(new MouseListener() {

					@Override
					public void mouseClicked(MouseEvent e) {
						gd=place.getText();
						nbc=Integer.parseInt(numcarte.getText());
						to_place=j.getMain().get(nbc);
						nbc2=Integer.parseInt(numcarte2.getText());
						current_card=p.played_cards.get(nbc2);
						if(gd.equals("g")) {
							if(current_card.test_date_gauche(((TimeLine)to_place).getDate())) {
								if(p.played_cards.indexOf(current_card)-1<0) {
									p.played_cards.add(0, to_place);
								} else {
									p.played_cards.add(p.played_cards.indexOf(current_card)-1, to_place);
								}							
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);						
								p.discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								p.discard_pile.add(to_place);
								p.pick_one_card(j);
							}

						}
						if(gd.equals("d")) {
							if(current_card.test_date_droite(((TimeLine)to_place).getDate())) {							
								p.played_cards.add(p.played_cards.indexOf(current_card)+1, to_place);
								System.out.println("La carte a �t� bien plac�e !");							
								j.poseUneCarte(to_place);
								p.discard_pile.add(to_place);
							} else {
								System.out.println("La date n'est pas la bonne.");
								j.poseUneCarte(to_place);
								p.pick_one_card(j);
							}
						}
					}

					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub

					}

				});
			
				win=true;
			}
			
			
		}



	}

	//m�thode pour afficher les cartes
	//avec une redimension pour les adapter � la taille du JLabel
	public void remplir_image(JLabel cont,String image_path) {		
		ImageIcon image=new ImageIcon(image_path);
		Image temp=image.getImage();
		temp=temp.getScaledInstance(90,150,java.awt.Image.SCALE_SMOOTH);
		cont.setText(null);
		image=new ImageIcon(temp);
		cont.setIcon(image);		
	}

	public String chemin_r(String s) {
		return ".\\data\\timeline\\cards\\"+s+".jpeg";
	}

	public String chemin_v(String s) {
		return ".\\data\\timeline\\cards\\"+s+"_date.jpeg";
	}
}
